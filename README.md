# Simple REST client #
## Installation ##
### CL ###
```
composer require ntorgov/RESTClient
```
### composer.json ###
```json
{
   "require": {
      "ntorgov/RESTClient": "*"
   }
}
```
## Usage ##
```php
<?php
$restClient = new \RESTClient\Client();
$restClient->setServiceURL('http://service.url/api/something/get');
$restClient->setMethod(\RESTClient\Method::GET);
$contentResult = $restClient->GetContent();
$jsonResult = $restClient->ParseJSON($contentResult);
```
It's simple