<?php

namespace RESTClient;

class Client
{
	/**
	 * URL
	 *
	 * @var
	 */
	private $serviceURL;
	private $method = 0;
	private $data;

	public function __construct()
	{
		$this->method = Method::GET;
	}

	public function ParseJSON($initialResult)
	{

		return json_decode($initialResult);
	}

	public function GetContent()
	{
		$curl = curl_init();
		$curlOptions = array();

		$curlOptions[CURLOPT_URL] = $this->getServiceURL();
		$curlOptions[CURLOPT_RETURNTRANSFER] = true;
		$curlOptions[CURLOPT_TIMEOUT] = 300;
		$curlOptions[CURLOPT_FOLLOWLOCATION] = false;

		if ($this->getMethod() === Method::POST) {
			$curlOptions[CURLOPT_POST] = true;
		}

		if ($this->getData()) {
			echo('Data is present: ' . $this->getData());
			$curlOptions[CURLOPT_POSTFIELDS] = $this->getData();
			$curlOptions[CURLOPT_HTTPHEADER] = 'Content-Type: application/json';
		}

		curl_setopt_array($curl, $curlOptions);
		$pageContent = curl_exec($curl);
		curl_close($curl);
		// print_r($pageContent);
		return $pageContent;
	}

	/**
	 * @return int
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @param int $method
	 *
	 * @return int
	 */
	public function setMethod($method)
	{
		$this->method = $method;
		return $this->method;
	}

	/**
	 * @return mixed
	 */
	public function getServiceURL()
	{
		return $this->serviceURL;
	}

	/**
	 * @param mixed $serviceURL
	 */
	public function setServiceURL($serviceURL)
	{
		$this->serviceURL = $serviceURL;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param mixed $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
}