<?php

namespace RESTClient;


/**
 * Class Method
 *
 * @package RESTClient
 */
class Method
{
	/**
	 * Метод GET
	 */
	const GET = 1;

	/**
	 * Метод POST
	 */
	const POST = 2;

	/**
	 * Метод PUT
	 */
	const PUT = 3;

	/**
	 * Метод PATCH
	 */
	const PATCH = 4;

	/**
	 * Метод DELETE
	 */
	const DELETE = 5;
}