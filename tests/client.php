<?php

use PHPUnit\Framework\TestCase;

class clientTest extends TestCase
{
	public function testSetMethods()
	{
		$restClient = new \RESTClient\Client;
		$methodValue = \RESTClient\Method::DELETE;
		$resultValue = $restClient->setMethod($methodValue);
		$this->assertEquals(
			$methodValue,
			$resultValue,
			'Set method error'
		);
	}

	public function testGetMethods()
	{
		$restClient = new \RESTClient\Client;
		$resultValue = $restClient->getMethod();
		$this->assertEquals(
			\RESTClient\Method::GET,
			$resultValue,
			'Get method error'
		);
	}

	public function testGetContent()
	{
		$restClient = new \RESTClient\Client();
		$restClient->setServiceURL('http://crm.agroprint.ru/api/shapes/get');
		$restClient->setMethod(\RESTClient\Method::GET);
		$result = $restClient->GetContent();
		$this->assertGreaterThan(
			100,
			strlen($result),
			'Get content error'
		);
	}

	public function testParseJSON()
	{
		$restClient = new \RESTClient\Client();
		$restClient->setServiceURL('http://crm.agroprint.ru/api/shapes/get');
		$restClient->setMethod(\RESTClient\Method::GET);
		$contentResult = $restClient->GetContent();
		$jsonResult = $restClient->ParseJSON($contentResult);
		$this->assertTrue(is_array($jsonResult), 'Parse content fail');
		$this->assertGreaterThan(15, strlen($jsonResult[0]->_id), 'Parse content fail');
	}

	public function testGetSubContent()
	{
		$restClient = new \RESTClient\Client();
		$restClient->setServiceURL('http://crm.agroprint.ru/api/shapes/get');
		$restClient->setMethod(\RESTClient\Method::GET);
		$contentResult = $restClient->GetContent();
		$jsonResult = $restClient->ParseJSON($contentResult);
		$this->assertTrue(is_array($jsonResult), 'Parse content fail');
		$this->assertGreaterThan(15, strlen($jsonResult[0]->_id), 'Parse content fail');

		$firstShape = $jsonResult[0]->_id;
		$restClient->setServiceURL('http://crm.agroprint.ru/api/knifes/get/' . $firstShape);
		$restClient->setMethod(\RESTClient\Method::POST);
		$contentResult = $restClient->GetContent();
		$jsonResult = $restClient->ParseJSON($contentResult);
		// print_r($jsonResult);
		$this->assertTrue(is_array($jsonResult), 'Parse content fail');
		$this->assertGreaterThan(15, strlen($jsonResult[0]->_id), 'Parse content fail');
		// print_r($firstShape);
	}

	public function testPostSend()
	{
		$postData = array(
			'colors'   => 3,
			'kinds'    => 1,
			'knife'    => 'jLTpq9L3wecFreNPd',
			'material' => array('id' => 'rKkxQ2mLYNxLNFyhP'),
			'pantones' => 0,
			'polish'   => 'eXiM2p4hYJvxgxJcf',
			'quantity' => 2500,
			'shape'    => 'oKZZtref3qAab2DyG',
		);
		$restClient = new \RESTClient\Client();
		$restClient->setServiceURL('http://crm.agroprint.ru/api/calculator/get/' . base64_encode(json_encode($postData)));
		// $restClient->setData(json_encode($postData));
		// $restClient->setMethod(\RESTClient\Method::GET);
		$contentResult = $restClient->GetContent();
		$jsonResult = $restClient->ParseJSON($contentResult);
		print_r($contentResult);
		$this->assertTrue(is_array($jsonResult), 'Parse content fail');
		$this->assertGreaterThan(15, strlen($jsonResult[0]->_id), 'Parse content fail');
	}
}
