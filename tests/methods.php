<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use RESTClient\Method;

class Methods extends TestCase
{
	public function testMethods()
	{
		$this->assertEquals(
			1,
			Method::GET,
			'Method error'
		);
	}
}
